class WhoLikesIt
  attr_reader :input

  def initialize(input)
    @input = input
  end

  def execute
    return unless input.is_a?(Array)

    generate_string
  end

  private

  def generate_string
    "#{people_string} #{like_string}"
  end

  def like_string
    "#{input.size > 1 ? "like" : "likes"} this"
  end

  def people_string
    response = input.size.zero? ? "No one" : "#{input.first}"

    case input.size
    when 0, 1
      response
    when 2
      "#{response} and #{input.last}"
    when 3
      "#{response}, #{input[1]} and #{input.last}"
    else
      "#{response}, #{input[1]} and #{input.size - 2} others"
    end
  end
end
