require_relative 'who_likes_it'
require 'test/unit'

class WhoLikesItTest < Test::Unit::TestCase
  def test_case_0
    input = []
    assert_equal "No one likes this", WhoLikesIt.new(input).execute
  end

  def test_case_1
    input = ["Urbi"]
    assert_equal "Urbi likes this", WhoLikesIt.new(input).execute
  end

  def test_case_2
    input = ["Urbi", "Cuit"]
    assert_equal "Urbi and Cuit like this", WhoLikesIt.new(input).execute
  end

  def test_case_3
    input = ["Urbi", "Cuit", "Rechicken"]
    assert_equal "Urbi, Cuit and Rechicken like this", WhoLikesIt.new(input).execute
  end

  def test_case_4
    input = ["Urbi", "Cuit", "Rechicken", "Llonpol"]
    assert_equal "Urbi, Cuit and 2 others like this", WhoLikesIt.new(input).execute
  end

end
